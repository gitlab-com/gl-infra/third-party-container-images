**THIS PROJECT HAS BEEN ARCHIVED**

Please use the images in https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks-images instead.

**THIS PROJECT HAS BEEN ARCHIVED**


# third-party-container-images

This is a tiny repository containing some commonly used third-party tools which don't have their own container images.

Maintaining our own images for these tools saves on having to maintain multiple build scripts across many different repositories.

Currently, the following tools are supported:

### Jsonnet Bundler, `jb`

```Dockerfile
FROM registry.gitlab.com/gitlab-com/gl-infra/third-party-container-images/jb:v${GL_ASDF_JB_VERSION} AS jb

# ...

COPY --from=jb /usr/bin/jb /usr/bin/jb
```

### Go-Jsonnet, `go-jsonnet`

```Dockerfile
FROM registry.gitlab.com/gitlab-com/gl-infra/third-party-container-images/go-jsonnet:v${GL_ASDF_GO_JSONNET_VERSION} AS go-jsonnet

# ...

COPY --from=go-jsonnet /usr/bin/jsonnet /usr/bin/jsonnet
COPY --from=go-jsonnet /usr/bin/jsonnetfmt /usr/bin/jsonnetfmt
```

## Hacking on third-party-container-images

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
